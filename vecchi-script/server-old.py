from flask import Flask, request, jsonify

app = Flask(__name__)

todos = [
    {"userId": 1, "id": 1, "title": "Fare la spesa", "completed": False},
    {"userId": 1, "id": 2, "title": "Studiare per l'esame", "completed": True},
    {"userId": 10, "id": 3, "title": "Riordinare gli appunti", "completed": True},
    {"userId": 10, "id": 4, "title": "Andare a correre", "completed": False}
]

def _find_next_id():
    return max(todo["id"] for todo in todos) + 1

def _find_todo_by_id(id):
    for todo in todos:
        if todo["id"] == int(id):
            return todo
    return None

# Recupera tutti i Todo
@app.get("/todos")
def get_todos():
    return jsonify(todos), 200

# Recupera un Todo dato il suo ID
@app.get("/todos/<id>")
def get_todo_by_id(id):
    return jsonify(_find_todo_by_id(id)), 200

# Crea un nuovo Todo
@app.post("/todos")
def add_todos():
    if request.is_json:
        todo = request.get_json()
        todo["id"] = _find_next_id()
        todos.append(todo)
        return todo, 201
    return {"error": "Request must be JSON"}, 415

# Modifica un Todo esistente
@app.put("/todos/<id>")
def put_todo(id):
    if request.is_json:
        todo = request.get_json()
        old_todo = _find_todo_by_id(id)
        todos.remove(old_todo)
        todos.append(todo)
        return todo, 201
    return {"error": "Request must be JSON"}, 415


# Elimina un Todo esistente
@app.delete("/todos/<id>")
def delete_todo(id):
    old_todo = _find_todo_by_id(id)
    todos.remove(old_todo)
    return {"message": "OK"}, 200
