import requests
import time

delay = 5

if __name__ == "__main__":
    api_url = "http://localhost:5000"
    my_headers = {"Content-type": "application/json"}

    # Recupero tutti i Todo
    print("GET /todos")
    res = requests.get(f"{api_url}/todos", headers=my_headers)
    print("Risposta dal server:")
    print(res.json())

    time.sleep(delay)

    # Aggiungo un nuovo Todo
    print("\n")
    print("POST /todos")
    to_insert = {
        "title": "Lavare i piatti",
        "completed": False,
        "userId": 123
    }
    res = requests.post(f"{api_url}/todos", headers=my_headers, json=to_insert)
    print("Risposta dal server:")
    print(res.json())
    # Mi salvo l'ID del Todo che ho creato
    created_id = res.json()["id"]

    time.sleep(delay)

    # Recupero il Todo appena creato
    print("\n")
    print(f"GET /todos/{created_id}")
    res = requests.get(f"{api_url}/todos/{created_id}", headers=my_headers)
    print("Risposta dal server:")
    print(res.json())

    time.sleep(delay)

    # Modifico il Todo che ho creato (lo sostituisco)
    print("\n")
    print(f"PUT /todos/{created_id}")
    to_change = {
        "id": created_id,
        "title": "Lavare i piatti",
        "completed": True,
        "userId": 123
    }
    res = requests.put(f"{api_url}/todos/{created_id}", headers=my_headers, json=to_change)
    print("Risposta dal server:")
    print(res.json())

    time.sleep(delay)

    """
    Su jsonplaceholder le risorse non vengono veramente inserite,
    modificate o eliminate, quindi quando vengono recuperate dopo
    una di queste azioni non si ha un riscontro di quanto fatto.
    Quando gli studenti implementeranno il server in locale, invece,
    si potrà recuperare la risorsa per vedere che è stata effettivamente
    inserita/modificata/eliminata.
    """

    # Recupero il Todo che ho modificato
    print("\n")
    print(f"GET /todos/{created_id}")
    res = requests.get(f"{api_url}/todos/{created_id}", headers=my_headers)
    print("Risposta dal server:")
    print(res.json())

    time.sleep(delay)

    # Elimino il Todo
    print("\n")
    print(f"DELETE /todos/{created_id}")
    res = requests.delete(f"{api_url}/todos/{created_id}", headers=my_headers)
    print("Risposta dal server:")
    print(res.json())

    time.sleep(delay)

    # Infine recupero tutti i Todo
    print("\n")
    print("GET /todos")
    res = requests.get(f"{api_url}/todos", headers=my_headers)
    print("Risposta dal server:")
    print(res.json())
