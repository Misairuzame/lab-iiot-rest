from flask import Flask, request, jsonify
import json
from bson.json_util import ObjectId
import IMongodb

class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, ObjectId):
            return str(obj)
        return super(MyEncoder, self).default(obj) 

app = Flask(__name__)
app.json_encoder = MyEncoder

IMongodb.init_db()

# Prima di servire qualsiasi richiesta,
# verificare che il Content-type sia
# application/json, se non lo è allora
# restituire un messaggio di errore
@app.before_request
def check_cont_type_json():
    if not request.is_json:
        return {"errore": "Content-type non supportato (deve essere JSON)"}, 415


# ----- TODOS -----


# Recupera tutti i Todo
@app.get("/todos")
def get_todos():
    return jsonify(IMongodb.find_all_todos()), 200


# Recupera un Todo dato il suo ID
# Se non lo trova, restituisce un
# oggetto JSON vuoto, ovvero {}, e
# risponde con stato HTTP 404
@app.get("/todos/<id>")
def get_todo_by_id(id):
    to_return = IMongodb.find_todo_by_id(id)
    if to_return is None:
        return "{}", 404
    else:
        return jsonify(to_return), 200


# Crea un nuovo Todo, del quale l'id
# viene impostato al successivo id disponibile
# (come se fosse un autoincrementante)
@app.post("/todos")
def add_todo():
    todo = request.get_json()
    todo["id"] = IMongodb.find_max_todo_id() + 1
    IMongodb.insert_todo(todo)
    return todo, 201


# Modifica/Sostituisce un Todo esistente,
# restituendo al Client la "nuova" risorsa
# e lo stato HTTP 201
@app.put("/todos/<id>")
def put_todo(id):
    todo = request.get_json()
    IMongodb.delete_todo_by_id(id)
    IMongodb.insert_todo(todo)
    return todo, 201


# Elimina un Todo esistente
@app.delete("/todos/<id>")
def delete_todo(id):
    IMongodb.delete_todo_by_id(id)
    return {"message": "OK"}, 200


# ----- USERS -----


# Recupera tutti gli utenti
@app.get("/users")
def get_users():
    return jsonify(IMongodb.find_all_users()), 200


# Recupera un utente dato il suo ID
# Se non lo trova, restituisce un
# oggetto JSON vuoto, ovvero {}, e
# risponde con stato HTTP 404
@app.get("/users/<id>")
def get_user_by_id(id):
    to_return = IMongodb.find_user_by_id(id)
    if to_return is None:
        return "{}", 404
    else:
        return jsonify(to_return), 200


# Crea un nuovo utente, del quale l'id
# viene impostato al successivo id disponibile
# (come se fosse un autoincrementante)
@app.post("/users")
def add_user():
    user = request.get_json()
    user["id"] = IMongodb.find_max_user_id() + 1
    IMongodb.insert_user(user)
    return user, 201


# Modifica/Sostituisce un utente esistente,
# restituendo al Client la "nuova" risorsa
# e lo stato HTTP 201
@app.put("/users/<id>")
def put_user(id):
    user = request.get_json()
    IMongodb.delete_user_by_id(id)
    IMongodb.insert_user(user)
    return user, 201


# Elimina un utente esistente
@app.delete("/users/<id>")
def delete_user(id):
    IMongodb.delete_user_by_id(id)
    return {"message": "OK"}, 200


# ----- USERS/<ID>/TODOS -----


# Restituisce tutti i todo appartenenti
# all'utente con l'id specificato
@app.get("/users/<id>/todos")
def get_todos_by_user(id):
    return jsonify(IMongodb.find_todos_by_user_id(id)), 200


# Aggiunge ai todo dell'utente con id
# specificato un nuovo todo
@app.post("/users/<id>/todos")
def add_todo_to_user(id):
    todo = request.get_json()
    todo["id"] = IMongodb.find_max_todo_id() + 1
    todo["userId"] = id
    IMongodb.insert_todo(todo)
    return todo, 201
