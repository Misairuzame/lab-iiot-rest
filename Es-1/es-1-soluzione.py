import requests

if __name__ == "__main__":
    api_url = "http://jsonplaceholder.typicode.com"
    my_headers = {"Content-type": "application/json"}

    # GET /todos
    # Recuperare tutti i Todo
    print("GET /todos")
    res = requests.get(f"{api_url}/todos", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # GET /todos/1
    # Recuperare il Todo con id 1
    print("\n")
    print("GET /todos/1")
    res = requests.get(f"{api_url}/todos/1", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # POST /todos
    # Aggiungere un nuovo Todo, il cui id verrà
    # assegnato automaticamente dal server
    print("\n")
    print("POST /todos")
    to_insert = {
        "title": "Lavare i piatti",
        "completed": False,
        "userId": 123
    }
    res = requests.post(f"{api_url}/todos", headers=my_headers, json=to_insert)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # PUT /todos/1
    # Modificare/Sostituire il Todo con id 1
    print("\n")
    print(f"PUT /todos/1")
    to_change = {
        "id": 1,
        "title": "Studiare per l'esame",
        "completed": True,
        "userId": 321
    }
    res = requests.put(f"{api_url}/todos/1", headers=my_headers, json=to_change)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # DELETE /todos/5
    # Eliminare il Todo con id 5
    print("\n")
    print(f"DELETE /todos/5")
    res = requests.delete(f"{api_url}/todos/5", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")
