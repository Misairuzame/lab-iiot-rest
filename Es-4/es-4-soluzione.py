from flask import Flask, request, jsonify

app = Flask(__name__)

todos = [
    {"userId": 1, "id": 1, "title": "Fare la spesa", "completed": False},
    {"userId": 1, "id": 2, "title": "Studiare per l'esame", "completed": True},
    {"userId": 2, "id": 3, "title": "Riordinare gli appunti", "completed": True},
    {"userId": 2, "id": 4, "title": "Andare a correre", "completed": False}
]


def _find_next_id():
    return max(todo["id"] for todo in todos) + 1

def _find_todo_by_id(id):
    for todo in todos:
        if todo["id"] == id or todo["id"] == int(id):
            return todo
    return None


# Prima di servire qualsiasi richiesta,
# verificare che il Content-type sia
# application/json, se non lo è allora
# restituire un messaggio di errore
@app.before_request
def check_cont_type_json():
    if not request.is_json:
        return {"errore": "Content-type non supportato (deve essere JSON)"}, 415


# Recupera tutti i Todo
@app.get("/todos")
def get_todos():
    return jsonify(todos), 200


# Recupera un Todo dato il suo ID
# Se non lo trova, restituisce un
# oggetto JSON vuoto, ovvero {}, e
# risponde con stato HTTP 404
@app.get("/todos/<id>")
def get_todo_by_id(id):
    to_return = _find_todo_by_id(id)
    if to_return is None:
        return "{}", 404
    else:
        return jsonify(to_return), 200


# Crea un nuovo Todo, del quale l'id
# viene impostato al successivo id disponibile
# (come se fosse un autoincrementante)
@app.post("/todos")
def add_todo():
    todo = request.get_json()
    todo["id"] = _find_next_id()
    todos.append(todo)
    return todo, 201


# Modifica/Sostituisce un Todo esistente,
# restituendo al Client la "nuova" risorsa
# e lo stato HTTP 201
@app.put("/todos/<id>")
def put_todo(id):
    todo = request.get_json()
    old_todo = _find_todo_by_id(id)
    todos.remove(old_todo)
    todos.append(todo)
    return todo, 201


# Elimina un Todo esistente
@app.delete("/todos/<id>")
def delete_todo(id):
    old_todo = _find_todo_by_id(id)
    todos.remove(old_todo)
    return {"message": "OK"}, 200
