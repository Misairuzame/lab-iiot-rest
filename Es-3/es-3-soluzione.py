import requests

if __name__ == "__main__":
    api_url = "http://jsonplaceholder.typicode.com"
    my_headers = {"Content-type": "application/json"}

    # ----- ESERCIZIO 1 -----

    # GET /todos
    # Recuperare tutti i Todo
    print("GET /todos")
    res = requests.get(f"{api_url}/todos", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # GET /todos/1
    # Recuperare il Todo con id 1
    print("\n")
    print("GET /todos/1")
    res = requests.get(f"{api_url}/todos/1", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # POST /todos
    # Aggiungere un nuovo Todo, il cui id verrà
    # assegnato automaticamente dal server
    print("\n")
    print("POST /todos")
    to_insert = {
        "title": "Lavare i piatti",
        "completed": False,
        "userId": 123
    }
    res = requests.post(f"{api_url}/todos", headers=my_headers, json=to_insert)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # PUT /todos/1
    # Modificare/Sostituire il Todo con id 1
    print("\n")
    print(f"PUT /todos/1")
    to_change = {
        "id": 1,
        "title": "Studiare per l'esame",
        "completed": True,
        "userId": 321
    }
    res = requests.put(f"{api_url}/todos/1", headers=my_headers, json=to_change)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # DELETE /todos/5
    # Eliminare il Todo con id 5
    print("\n")
    print(f"DELETE /todos/5")
    res = requests.delete(f"{api_url}/todos/5", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # ----- ESERCIZIO 2 -----

    # GET /users/1
    # Recuperare l'utente con id 1
    print("\n")
    print("GET /users/1")
    res = requests.get(f"{api_url}/users/1", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # POST /users
    # Aggiungere un nuovo utente, il cui id verrà
    # assegnato automaticamente dal server
    print("\n")
    print("POST /users")
    to_insert = {
        "name": "Mario Rossi",
        "username": "MR",
        "email": "mario.rossi@test.com",
        "address": {
            "street": "Via Saragat",
            "suite": "Apt. 1",
            "city": "Ferrara",
            "zipcode": "44122",
            "geo": {
                "lat": "44.8333129",
                "lng": "11.5973502"
            }
        },
        "phone": "123-456-789",
        "website": "unife.it",
        "company": {
            "name": "Università degli Studi di Ferrara",
            "catchPhrase": "ReST",
            "bs": "API"
        }
    }
    res = requests.post(f"{api_url}/users", headers=my_headers, json=to_insert)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # PUT /users/1
    # Modificare/Sostituire l'utente con id 1
    print("\n")
    print(f"PUT /users/1")
    to_change = {
        "id": "1",
        "name": "Luigi Bianchi",
        "username": "LB",
        "email": "luigi.bianchi@test.com",
        "address": {
            "street": "Via Saragat",
            "suite": "Apt. 13",
            "city": "Ferrara",
            "zipcode": "44122",
            "geo": {
                "lat": "44.8312366",
                "lng": "11.5971991"
            }
        },
        "phone": "987-654-321",
        "website": "unife.it",
        "company": {
            "name": "Università degli Studi di Ferrara",
            "catchPhrase": "ReST",
            "bs": "API"
        }
    }
    res = requests.put(f"{api_url}/users/1", headers=my_headers, json=to_change)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # ----- ESERCIZIO 3 -----

    # GET /users/1/todos
    # Recuperare i todo dell'utente con id 1
    print("\n")
    print("GET /users/1/todos")
    res = requests.get(f"{api_url}/users/1/todos", headers=my_headers)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")

    # POST /users/1/todos
    # Aggiungere un nuovo todo per l'utente 1,
    # il cui id verrà assegnato automaticamente
    # dal server
    print("\n")
    print("POST /users/1/todos")
    to_insert = {
        "title": "Riordinare gli appunti",
        "completed": True,
        "userId": 1
    }
    res = requests.post(f"{api_url}/users/1/todos", headers=my_headers, json=to_insert)
    print(f"Stato HTTP: {res.status_code}")
    print(f"Risposta dal server:\n{res.json()}")
